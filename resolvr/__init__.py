#Application global vars
VERSION = "1.0"
PROG_NAME = "resolvr"
PROG_DESC = "A penetration testing tool to resolve domains and optionally filter on in-scope results. Used after subdomain discovery to determine which hosts are actually targets and which are owned by third parties."
PROG_EPILOG = "Written by TheTwitchy. Source code at https://gitlab.com/TheTwitchy/resolvr"
DEBUG = False
